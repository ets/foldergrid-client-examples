<?php
class FolderGridClient {
         protected $_cookieFileLocation = './cookie.txt';
         public $_webpage;
         public $_status;
         public $_location;

        const SERVER_HOST = 'https://secure.foldergrid.com';

        public function authenticate($username,$password, $domain){
            $params = array("domain"=>$domain,"username"=>$username,"password"=>$password);
            $this->createCurl(self::SERVER_HOST."/login",$params);
        }
        public function createDomain($adminemail,$adminpassword, $domainname, $adminfname, $adminlname){
            $json = json_encode( array('invite'=>true, 'admin' => array('email'=>$adminemail,'password'=>$adminpassword,'firstname'=>$adminfname,'lastname'=>$adminlname) ) );
            $this->createCurl(self::SERVER_HOST."/domain/".$domainname,$json,true);
        }
        public function createFolder($duid,$folderName,$parentDuid,$subFoldersInherit){
                $json = json_encode( array('name'=>$folderName, 'parentDuid'=>$parentDuid, 'subfoldersInherit'=>$subFoldersInherit ) );
                $this->createCurl(self::SERVER_HOST."/folder/".$duid,$json,true);
        }
        public function disableUser($email){
                $json = json_encode( array('email'=>$email, 'enabled'=>'false') );
                $this->createCurl(self::SERVER_HOST."/user/".$email,$json,true);
        }
        public function uploadFile($file,$name,$parentDuid) {
            $fh = fopen($file, "rb");
            if($fh) {
                $json = json_encode( array('parentDuid'=>$parentDuid, 'name' => $name, 'parts' => 1) );
                $this->createCurl(self::SERVER_HOST."/file/provision",$json,true);

                if($this->_location){
                    $headers = array(
                            'fg-eap: false',
                            'fg-md5:'.base64_encode(md5_file( $file ,true)),
                            'Content-Type: binary/octet-stream'
                    );

                    $curl = curl_init();

                    curl_setopt($curl,CURLOPT_PUT,true);
                    curl_setopt($curl, CURLOPT_HEADER, TRUE);
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
                    curl_setopt($curl, CURLOPT_URL, $this->_location);
                    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($curl, CURLOPT_BINARYTRANSFER, true);
                    curl_setopt($curl, CURLOPT_INFILE, $fh);
                    curl_setopt($curl, CURLOPT_INFILESIZE, filesize($file));
                    curl_setopt( $curl, CURLOPT_VERBOSE, true );

                    $this->_webpage = curl_exec($curl);
                    $this->_status = curl_getinfo($curl,CURLINFO_HTTP_CODE);
                    fclose($fh);
                    curl_close($curl);
                }
            }else{
                echo "File not found: $file \n";
            }
        }
        public function fetchFolder($duid){
            $this->createCurl(self::SERVER_HOST."/folder/$duid");
            return $this->_webpage;
        }
        public function fetchFile($fileid){
            $this->createCurl(self::SERVER_HOST."/file/$fileid");
            return $this->_webpage;
        }

        public function createCurl($url,$postFields = null, $put = false)
         {
             $s = curl_init();

             curl_setopt($s, CURLOPT_VERBOSE, true);
             curl_setopt($s,CURLOPT_URL,$url);
             curl_setopt($s,CURLOPT_RETURNTRANSFER,true);
             curl_setopt($s, CURLOPT_HEADER, TRUE);
             curl_setopt($s,CURLOPT_FOLLOWLOCATION,false);
             curl_setopt($s,CURLOPT_COOKIEJAR,$this->_cookieFileLocation);
             curl_setopt($s,CURLOPT_COOKIEFILE,$this->_cookieFileLocation);

             if($postFields)
             {
                 curl_setopt($s,CURLOPT_POST,true);
                 if($put) {
                    curl_setopt($s, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: ' . strlen($postFields)));
                    curl_setopt($s,CURLOPT_CUSTOMREQUEST, "PUT");
                 }else{
                    curl_setopt($s, CURLOPT_HTTPHEADER, array('Expect:'));
                 }
                 curl_setopt($s,CURLOPT_POSTFIELDS,$postFields);
             }
             $this->_webpage = curl_exec($s);
             $this->_status = curl_getinfo($s,CURLINFO_HTTP_CODE);
             preg_match_all('/^Location:(.*)$/mi', $this->_webpage, $matches);
             $this->_location = !empty($matches[1]) ? trim($matches[1][0]) : null;
             curl_close($s);
         }

    }
}

    $client = new FolderGridClient;
    $client->authenticate( $argv[1],$argv[2],$argv[3]);
    if($client->_status < 400){
        $client->uploadFile($argv[4],$argv[5],$argv[6]);
    }
?>