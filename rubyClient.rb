require 'rubygems' if RUBY_VERSION < '1.9'
require 'rest-client'
require 'json'
require 'securerandom'
require 'digest'

API_ENDPOINT = "https://secure.foldergrid.com/"

class FGClient

	attr_accessor :domain, :username

	def create_duid
		SecureRandom.uuid
	end

	def authenticate(domain, username, password)
	  @domain = domain
		@username = username
	  
		values = {
			:password => password,
			:username => @username,
			:domain => @domain
			}

		headers = {
			:content_type => 'application/x-www-form-urlencoded'
		}
		
		response = RestClient.post(
			API_ENDPOINT+"login", 
			values, headers
			) { |response, request, result, &block|
				case response.code
					when 302					  
					  if response.cookies.has_key?("GRID_SESSION") 
					    p "Successfully Authenticated"
					    @session_cookies = response.cookies
				    else
				      p "Auth failed"
				      p response.headers[:reason_phrase]
				    end					  
					else
					  p response.headers[:reason_phrase]
					  response
					end
				}

	end

	def read_folder(duid)

		headers = {
			:cookies => @session_cookies, :accept => 'application/json'
		}

		response = RestClient.get(API_ENDPOINT+"folder/#{duid}", headers){ |response, request, result, &block|
      case response.code
      when 200
        p "Successfully retrieved listing for #{duid}"
        response
      else
        p response.headers[:reason_phrase]
        response
      end
    }    
	end
	def download_file(id)

		headers = {
			:cookies => @session_cookies, :accept => 'application/json'
		}

		response = RestClient.get(API_ENDPOINT+"file/#{id}", headers){ |response, request, result, &block|
      case response.code
      when 302        
        response = RestClient.get(response.headers[:location])
      else
        p response.headers[:reason_phrase]
        response
      end
    }    
	end

	def create_folder(folder_name, parent_duid, new_duid)

		values = {
			"name" => "#{folder_name}",
			"parentDuid" => "#{parent_duid}",
			"subfolderInheritance" => true
		}.to_s

		headers = {
			:cookies => @session_cookies, :accept => 'application/json'
		}

		response = RestClient.put(
			API_ENDPOINT+"folder/#{new_duid}",
			values, headers
			) { |response, request, result, &block|
				case response.code
					when 200
					  p "Successfully Created a Folder named #{folder_name}!"
					  response
					else
					  p response.headers[:reason_phrase]
					  response
					end
				}
	end

	def provision_file(data_file, duid)
	  file = File.open(File.expand_path(data_file), "rb")
    file_name = File.basename(data_file, ".txt")    
		@md5 = Digest::MD5.hexdigest(file.read)
		file.close
		
		@epoch_time = DateTime.now.strftime('%Q')

		values = { 
			"name" => "#{file_name}",
			"parentDuid" => "#{duid}",
			"source-updated" => "#{@epoch_time}",
			"source-created" => "#{@epoch_time}",
			"md5" => "#{@md5}",
			"comment" => "optional comment in file metadata",
			"lock" => true
		}.to_s

		headers = {
			:cookies => @session_cookies, :content_type => 'application/json'
		}

		response = RestClient.put(
			API_ENDPOINT+'file/provision',
			 values, headers
			 ) { |response, request, result, &block|
				case response.code
					when 302
					  p "Your file #{file_name} will be uploaded shortly..."
						@upload_location = "" << response.headers[:location]
						response
					when 403
					  p "You do not have WRITE permission on the Folder identified by parentDuid."
					else
					  p response.headers[:reason_phrase]
					  response
					end
				}
	end
	def streaming_upload_file(data_file)
    file = File.open(File.expand_path(data_file), "rb")
		headers = {
		  "Content-Type" => "binary/octet-stream",
			"fg-ea" => false,
			"fg-md5" => @md5
		}
		response = RestClient.put(
			@upload_location,
			 file, headers
			 ) { |response, request, result, &block|
				case response.code
 					when 200
 					  p "Upload successful!"
 					  response
 					when 404
 						p "The given URL is not provisioned for receiving an upload."
 						p response.headers			
 					else
 					  p request.processed_headers
 					  p response.headers[:reason_phrase]
 					  response
 					end
 			}

	end

end


client = FGClient.new()
client.authenticate("yourdomain","auser@domain.com","supersecret")
target_duid = "~"  
data_file = './testupload.txt'

provision_response = client.provision_file(data_file, target_duid)
uploaded_file_id = provision_response.headers[:provisioned_id]
client.streaming_upload_file(data_file)
p "Newly uploaded file was assigned id= #{uploaded_file_id}"
file_content = client.download_file(uploaded_file_id)
p file_content
# p client.read_folder(target_duid)
