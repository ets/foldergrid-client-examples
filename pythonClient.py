#!/usr/bin/python

import urllib2, urllib, cookielib
import json

cj = cookielib.CookieJar()
opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cj))

login_data = urllib.urlencode({'username' : 'joesmith@mydomain.com', 'domain' : 'mydomain', 'password' : 'secretpassword'})
opener.open('https://secure.foldergrid.com/login', login_data)

file_name = 'ClientUsingCookies.result'
u = opener.open('https://secure.foldergrid.com/file/3')
f = open(file_name, 'wb')
meta = u.info()
file_size = int(meta.getheaders("Content-Length")[0])
print "Downloading: %s Bytes: %s" % (file_name, file_size)

file_size_dl = 0
block_sz = 8192
while True:
    buffer = u.read(block_sz)
    if not buffer:
        break

    file_size_dl += len(buffer)
    f.write(buffer)
    status = r"%10d  [%3.2f%%]" % (file_size_dl, file_size_dl * 100. / file_size)
    status = status + chr(8)*(len(status)+1)
    print status,

f.close()