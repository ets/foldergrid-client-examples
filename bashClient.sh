#!/bin/bash

# Configuration Variables
FG_USERNAME="joesmith@mydomain.com"
FG_PASSWORD="secretpassword"
FG_DOMAIN="mydomain"
FG_DUID="04hs03e94-e234-39e9-825c-56dlfdd12571"
FG_BASE_URI="https://secure.foldergrid.com"

# First send login info and get the
# right cookies and headers
curl --data "username=${FG_USERNAME}&domain=${FG_DOMAIN}&password=${FG_PASSWORD}" --dump-header /tmp/${FG_DOMAIN}_headers --cookie-jar /tmp/${FG_DOMAIN}_cookies.txt ${FG_BASE_URI}/login >/dev/null 2>&1

# Inspect the folder identified by $FG_DUID
# the python at the end isn't necessary but
# formats the json results better
curl --cookie /tmp/${FG_DOMAIN}_cookies.txt -L -b /tmp/${FG_DOMAIN}_headers ${FG_BASE_URI}/folder/${FG_DUID} 2>/dev/null| grep }| python -mjson.tool

# remove headers and cookies when we're done
rm -rf /tmp/${FG_DOMAIN}_cookies.txt /tmp/${FG_DOMAIN}_headers