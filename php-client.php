<?php
date_default_timezone_set('America/New_York');
require 'vendor/autoload.php';
$server = "https://secure.foldergrid.com";
$username = $argv[1];
$password = $argv[2];
$domain = $argv[3];
$uri = "$server/login";
$client = new \GuzzleHttp\Client(['cookies' => true]);
$r = $client->request('POST', $uri, [
    'form_params' => [
        'domain' => $domain,
        'username' => $username,
        'password' => $password
    ],
    'allow_redirects' => false
]);
if($r->getStatusCode() < 400){
  echo "folder\n";
  $uri = "$server/folder/~";
  $response = $client->head($uri);
  foreach ($response->getHeaders() as $name => $values) {
    echo $name . ': ' . implode(', ', $values) . "\r\n";
  }
  echo $response->getBody();
}

?>
