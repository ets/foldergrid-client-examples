import com.google.gson.Gson;
import com.ning.http.client.AsyncHttpClient;
import com.ning.http.client.ProxyServer;
import com.ning.http.client.AsyncHttpClient.BoundRequestBuilder;
import com.ning.http.client.AsyncHttpClientConfig;
import com.ning.http.client.AsyncHttpClientConfig.Builder;
import com.ning.http.client.generators.InputStreamBodyGenerator;

public class Client {
    private AsyncHttpClient asyncClient = null;
    private Cookie authenticatedCookie = null;

    public Client(){
        final Builder bc = new AsyncHttpClientConfig.Builder();
        bc.setFollowRedirects(false);
        bc.setUseRawUrl(false);
        bc.setAllowPoolingConnection(true);
        bc.setAllowSslConnectionPool(true);
        bc.setRequestCompressionLevel(9);
        bc.setCompressionEnabled(true);
        asyncClient = new AsyncHttpClient(new JDKAsyncHttpProvider(bc.build()));
    }
    public final boolean authenticate(final String domain,final String user,final String password)  {
        BoundRequestBuilder builder = asyncClient.preparePost("https://secure.foldergrid.com/login");
        builder.addParameter("username", user);
        builder.addParameter("domain", domain);
        builder.addParameter("password", password);

        Response resp;
        try {
            resp = builder.execute().get();
            if(resp.getStatusCode() / 100 == 3){
                for(Cookie c:resp.getCookies()){
                    if(c.getName().equals("GRID_SESSION") && c.getValue() != null && c.getValue().contains(username)){
                        authenticatedCookie = c;
                        return true;
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Unable to complete authentication.",e);
        }
        return false;
    }
    public JsonObject fetchFolder(String duid) Exception {
        BoundRequestBuilder builder = asyncClient.prepareGet("https://secure.foldergrid.com/folder/" + duid);
        builder.addCookie(authenticatedCookie);
        Response resp = builder.execute().get();
        if (resp.getStatusCode() / 100 == 2) {
            JsonElement body = new JsonParser().parse(resp.getResponseBody());
            updateAuthCookie(resp);
            return body.getAsJsonObject();
        } else {
            JsonObject obj = new JsonObject();
            obj.add("isReadable", new JsonPrimitive(false));
            return obj;
        }
    }
}