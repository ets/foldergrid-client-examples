using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Security.Cryptography;
using System.IO;
using System.IO.Compression;
using Microsoft.Win32.SafeHandles;
using System.Threading;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

// Note that this sample client does not include all class definitions (the FolderGridFile class for encapsulating File metadata should be implemented based upon contextual needs) and thus does not compile
namespace FolderGridWinMount
{   
    //Custom WebClient supporting reused headers and cookies, compression, and API request rate throttling   
    public class FGWebClient : WebClient 
    {
        private string userAgent = @"Custom FolderGrid WebClient v0.1";
        private System.Net.CookieContainer cookieContainer;
        private WebHeaderCollection headers;

        public FGWebClient(System.Net.CookieContainer cc, WebHeaderCollection h) {
            this.cookieContainer = cc;
            headers = h;
        }
        public WebRequest createRequest(Uri address) {
            return GetWebRequest(address);
        }

        DateTime lastRequestTime = DateTime.UtcNow;
        const double minimumRequestSpacingMS = 150.0; //about 6 requests per second allowed
        const int REQUEST_TIMEOUT = 30 * 1000;

        [MethodImpl(MethodImplOptions.Synchronized)]
        protected override WebRequest GetWebRequest(Uri address) {
            WebRequest request = base.GetWebRequest(address);
            if (request.GetType() == typeof(HttpWebRequest)) {
                HttpWebRequest webreq = ((HttpWebRequest)request);
                webreq.CookieContainer = cookieContainer;
                webreq.Headers.Add(headers);
                webreq.UserAgent = userAgent;
                webreq.AllowAutoRedirect = true;
                webreq.ProtocolVersion = System.Net.HttpVersion.Version11;
                webreq.SendChunked = false;
                webreq.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip,deflate");
                webreq.AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;
                webreq.Timeout = REQUEST_TIMEOUT;
                webreq.AllowWriteStreamBuffering = false;
            }
            double requiredWaitMS = minimumRequestSpacingMS - (DateTime.UtcNow - lastRequestTime).TotalMilliseconds;
            if (requiredWaitMS > 0) {
                Thread.Sleep(TimeSpan.FromMilliseconds(requiredWaitMS));
            }
            lastRequestTime = DateTime.UtcNow;
            return request;

        }
    }
    //Remove timeouts for uploads/downloads
    public static class WebRequestExtensions
    {
        public static Stream GetRequestStreamWithTimeout(
            this WebRequest request,
            int? millisecondsTimeout = null)
        {
            return AsyncToSyncWithTimeout(
                request.BeginGetRequestStream,
                request.EndGetRequestStream,
                millisecondsTimeout ?? request.Timeout);
        }

        public static WebResponse GetResponseWithTimeout(
            this WebRequest request,
            int? millisecondsTimeout = null)
        {
            return AsyncToSyncWithTimeout(
                request.BeginGetResponse,
                request.EndGetResponse,
                millisecondsTimeout ?? request.Timeout);
        }

        private static T AsyncToSyncWithTimeout<T>(
            Func<AsyncCallback, object, IAsyncResult> begin,
            Func<IAsyncResult, T> end,
            int millisecondsTimeout)
        {
            var iar = begin(null, null);
            if (!iar.AsyncWaitHandle.WaitOne(millisecondsTimeout))
            {
                var ex = new TimeoutException();
                throw new WebException(ex.Message, ex, WebExceptionStatus.Timeout, null);
            }
            return end(iar);
        }
    }
    //Implement methods for consuming FolderGrid's API
    public class FGClient
    {        
        private string PORTAL_HOST = @"https://secure.foldergrid.com/";
        private WebHeaderCollection headers = new WebHeaderCollection();        
        private System.Net.CookieContainer cookieContainer = new CookieContainer();        

        public FGClient(){       
        }
        
        //Do not reuse WebClient instances - instead share state across multiple instances
        private FGWebClient getWebClient(){
            FGWebClient webclient = new FGWebClient(this.cookieContainer,this.headers);
            return webclient;
        }

        
        //Use cookies to verify authentication
        public bool authenticate(String domain,String user,String password)  {
            Boolean result = false;
            
            string postData = String.Format("username={0}&domain={1}&password={2}", user,domain,password);            
            try {
                
                    WebRequest request = getWebClient().createRequest(new Uri(PORTAL_HOST + "login"));
                    request.AllowAutoRedirect = false;
                    request.Method = "POST";
                    request.ContentType = "application/x-www-form-urlencoded";
                    ((HttpWebRequest)request).SendChunked = false;
                    byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                    request.ContentLength = byteArray.Length;
                    using (var stream = request.GetRequestStream()) {
                        stream.Write(byteArray, 0, byteArray.Length);
                    }
                    using (var response = (HttpWebResponse)request.GetResponse()) {//don't use wrapped call here as we expect a WebException on any failure
                        foreach (Cookie cookie in cookieContainer.GetCookies(new Uri(PORTAL_HOST))) {
                            if (cookie.Name.Equals("GRID_SESSION") && cookie.Value != null && cookie.Value.Contains("userid")) {                                
                                result = true;
                                break;
                            }
                        }
                    }
                
            }
            catch (WebException we) {
                Debug("Unable to authenticate: " + we.Message);
            }
                        
            return result;    
        }        
        
        private void refreshContent(FolderGridFile currentFolder) 
        {
            HttpWebRequest request = (HttpWebRequest)getWebClient().createRequest(new Uri(PORTAL_HOST + "folder/" + currentFolder.duid));
            request.Method = "GET";
            if (currentFolder.eTag != null) {
                request.Headers["If-None-Match"] = currentFolder.eTag;
            }
            try {
                using (var response = getWrappedHttpResponse(request)) {                   
                    if (response.StatusCode == HttpStatusCode.OK) {
                        using (Stream source = response.GetResponseStream()) {
                            currentFolder.populate(JObject.Parse(new StreamReader(source).ReadToEnd()));
                            currentFolder.lastRefreshed = DateTime.UtcNow;
                            currentFolder.eTag = response.Headers.Get("ETag");
                        }
                    }
                    else if (response.StatusCode == HttpStatusCode.NotModified) {
                        //Debug("Folder not modified since last full read.");
                        currentFolder.lastRefreshed = DateTime.UtcNow;                         
                    }
                }
            }
            catch (WebException e) {
                Debug("Unable to refresh folder. "+e.Message);
            }
         
        }
        public HttpWebResponse getWrappedHttpResponse(WebRequest request) {
            try {                               
                return (HttpWebResponse)request.GetResponse();
            }
            catch (WebException ex) {
                var response = (HttpWebResponse)ex.Response;
                if(ex.Status == WebExceptionStatus.ProtocolError){ //workaround for support of HttpStatusCode.NotModified
                    return response;
                }

                if (response != null) 
                {
                    if(response.StatusCode == HttpStatusCode.MethodNotAllowed || response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.NotFound ||
                        response.StatusCode == HttpStatusCode.Forbidden || response.StatusCode == HttpStatusCode.Found)
                    { //list of responses we intend to handle normally (do not want thrown as exceptions)
                        return response;
                    }
                }
                throw ex;
            }
        }
        public JObject fetchFolderContent(String duid) {
            return JObject.Parse(getWebClient().DownloadString(PORTAL_HOST + "folder/" + duid));
        }

        public bool mkdir(FolderGridFile target) {
            if (target != null) {
                target.duid = Guid.NewGuid().ToString();

                Dictionary<String, String> map = new Dictionary<String, String>();
                map.Add("parentDuid", target.parent.duid);
                map.Add("name", target.name);

                getWebClient().Headers.Set("content-type", "application/json");
                try {
                    getWebClient().UploadData(PORTAL_HOST + "folder/" + target.duid, "PUT", Encoding.Default.GetBytes(JsonConvert.SerializeObject(map)));                                        
                    target.parent.addFolder(target);
                    return true;                    
                }
                catch (Exception) {
                    target.isWritable = false;
                    target.isRemovable = false;
                }                                 
            
            }
            return false;
        }        

        public bool fetchFileContent(FolderGridFile target, FileStream targetBuffer) {
            bool success = false;            
            WebRequest request = getWebClient().createRequest(new Uri(PORTAL_HOST + "file/" + target.id ));
            request.Method = "GET";
            Debug("Fetching: " + target.name);
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponseWithTimeout(10000))
            {
                if (response.StatusCode == HttpStatusCode.OK) {
                    using (var source = response.GetResponseStream()) {
                        source.CopyTo(targetBuffer);
                        targetBuffer.Flush();                                               
                        success = true;
                    }
                }
                else {                    
                    if (response.StatusCode == HttpStatusCode.Forbidden || response.StatusCode == HttpStatusCode.Unauthorized) {
                        target.isReadable = false;
                    }
                }
            }                       
            return success;
        }
        
        

        internal bool delete(FolderGridFile target) {
            WebRequest request = null;
            try {
                if (target.isDirectory()) {
                    request = getWebClient().createRequest(new Uri(PORTAL_HOST + "folder/" + target.duid));
                }
                else {
                    request = getWebClient().createRequest(new Uri(PORTAL_HOST + "file/" + target.id));
                }
                request.Method = "DELETE";
                using (var response = getWrappedHttpResponse(request))
                {
                    if (response.StatusCode == HttpStatusCode.OK) {
                        return true;
                    }
                    else {
                        target.isRemovable = false;
                    }
                }
            }
            catch {
                target.isRemovable = false;
            }
            return false;            
        }

        internal bool move(FolderGridFile src, FolderGridFile targetFolder, String newname) {
            try {
                if (src != null && targetFolder != null) {
                    Dictionary<String, String> map = new Dictionary<String, String>();
                    map.Add("parentDuid", targetFolder.duid);
                    map.Add("name", newname);

                    getWebClient().Headers.Set("content-type", "application/json");
                    String uriPart = src.isDirectory() ? "folder/" + src.duid : "file/" + src.id;
                    var json = getWebClient().UploadData(PORTAL_HOST + uriPart, "PUT", Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(map)));
                    src.name = newname;
                    if (src.isDirectory()) {
                        src.parent.removeFolder(src);
                        targetFolder.addFolder(src);
                    }
                    else {
                        src.parent.removeFile(src);
                        targetFolder.addFile(src);
                    }
                    src.parent = targetFolder;
                    return true;                            
                }                
            }
            catch (WebException we) {
                Debug("Unable to complete Move operation: "+we);
            }
            return false;
        }
        
        const long MAX_SIZE_PART_BYTES = 200000000000L; //literal max is 214,748,364,800 - so this value is conservative
        const int READ_BUFFER_SIZE = 65536;
        
        internal bool putFileContent(FolderGridFile target, Stream buffer)
        {                        
            long contentLength = buffer.Length;            
            if (target.md5 == null || !target.md5.Equals(target.bufferMD5Hash)) {
                //Debug("Buffer for {0} has changed - uploading content.",target.name);                
                try
                {
                    String provisionedUrl = provisionFile(target, target.bufferMD5Hash, buffer);
                    if (provisionedUrl == null)
                    {
                        return false; //Unable to provision file
                    }
                    if (contentLength > 0)
                    {
                        try
                        {
                            // Support multi-part large uploads                      
                            int partNum = 1;
                            int position = 0;
                            int retryCount = 0;
                            buffer.Seek(0, SeekOrigin.Begin);
                            int numParts = 1;
                            int remainder = 0;
                            if (contentLength > MAX_SIZE_PART_BYTES)
                            {
                                numParts = (int)(contentLength / MAX_SIZE_PART_BYTES);
                                remainder = (int)(contentLength % MAX_SIZE_PART_BYTES);
                                if (remainder > 0)
                                {
                                    numParts += 1;
                                }
                            }

                            while (position < contentLength)
                            {
                                int partSize = (int)Math.Min(MAX_SIZE_PART_BYTES, (contentLength - position));
                                HttpWebRequest request = (HttpWebRequest)getWebClient().createRequest(new Uri(provisionedUrl + "/" + partNum));
                                request.Method = "POST";
                                request.ContentType = "binary/octet-stream";
                                request.ContentLength = partSize;
                                request.Timeout = Timeout.Infinite;
                                request.ReadWriteTimeout = 30000;
                                if (partNum < numParts)
                                {
                                    request.Headers.Add("fg-eap", "true");
                                }
                                using (Stream postStream = request.GetRequestStreamWithTimeout(10000))
                                {
                                    int writeCount = 0;
                                    byte[] readBuffer = new Byte[READ_BUFFER_SIZE];
                                    while (writeCount < partSize)
                                    {
                                        int bytesRead = buffer.Read(readBuffer, 0, (int)Math.Min(READ_BUFFER_SIZE, partSize - writeCount));
                                        postStream.Write(readBuffer, 0, bytesRead);
                                        writeCount += bytesRead;
                                    }
                                }
                                using (HttpWebResponse response = (HttpWebResponse)request.GetResponseWithTimeout(10000))
                                {
                                    if (response.StatusCode == HttpStatusCode.OK)
                                    {
                                        position += partSize;
                                        partNum++;
                                        retryCount = 0;
                                    }
                                    else if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                                    {
                                        target.isWritable = false;
                                        return false;
                                    }
                                    else if (retryCount > 3)
                                    {
                                        throw new WebException("Unable to complete upload after three attempts.");
                                    }
                                    else
                                    {
                                        retryCount++;
                                    }
                                }
                            }
                            target.size = contentLength; //We update metadata size as we write to the buffer, but it can be refreshed with the server at any time - so sync here
                            return true;
                        }
                        catch (Exception we)
                        {                                                     
                            Debug("Unable to upload buffer content: " + we.Message);
                        }
                    }
                    else
                    {
                        return true;
                    }
                }
                catch (ConflictException)
                {
                    return true; //This revision is a dupe
                }
                catch (Exception e)
                {
                    Debug("Unable to provision upload: " + e.Message);
                }
                return false;
            }
            else {
                return true; //No changes in buffer
            }                                     
        }

        private String provisionFile(FolderGridFile target, String bufferMD5, Stream buffer)
        {
            Dictionary<String, String> map = new Dictionary<String, String>();
            map.Add("parentDuid", target.parent.duid);
            map.Add("name", target.name);
            map.Add("md5", bufferMD5);
            map.Add("source-updated", FolderGridFile.ToUnixTime(target.lastModified).ToString());
            map.Add("source-created", FolderGridFile.ToUnixTime(target.created).ToString());           
            
                bool exists = target.md5 != null;
                HttpWebRequest request = (HttpWebRequest)getWebClient().createRequest(new Uri(PORTAL_HOST + "file/provision?x-http-method-override=PUT"));
                request.AllowAutoRedirect = false;
                request.Method = "POST";
                request.ContentType = "application/json";
                var json = JsonConvert.SerializeObject(map);
                byte[] byteArray = Encoding.UTF8.GetBytes(json);
                request.ContentLength = byteArray.Length;
                using (var stream = request.GetRequestStream()) {
                    stream.Write(byteArray, 0, byteArray.Length);
                }
                using (var response = getWrappedHttpResponse(request))
                {
                    if (response.StatusCode == HttpStatusCode.Found) {
                        String provisionedUrl = response.Headers.Get("location");
                        long provisionedId = long.Parse(response.Headers.Get("provisioned-id"));
                        target.id = provisionedId;                        
                        return provisionedUrl;
                    }
                    else if (response.StatusCode == HttpStatusCode.Conflict)
                    {
                        throw new ConflictException();
                    }
                    else {
                        if (exists) {
                            target.isWritable = false;
                        }
                        else {
                            target.parent.isWritable = false;
                        }
                    }
                }
                return null;           
            
        }
       
    }
}
